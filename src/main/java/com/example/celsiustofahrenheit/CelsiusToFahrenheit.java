package com.example.celsiustofahrenheit;

public class CelsiusToFahrenheit {


    public double convert(String celsius) {
        return (Double.parseDouble(celsius) * (9 / 5)) + 32;
    }
}
