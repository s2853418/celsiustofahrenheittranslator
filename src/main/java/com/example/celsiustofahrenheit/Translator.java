package com.example.celsiustofahrenheit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Translator extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private CelsiusToFahrenheit celsiusToFahrenheit;

    public void init() throws ServletException {
        celsiusToFahrenheit = new CelsiusToFahrenheit();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius to Fahrenheit Translator";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                Double.toString(celsiusToFahrenheit.convert(request.getParameter("isbn"))) +
                "</BODY></HTML>");
    }
}
